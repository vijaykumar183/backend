from rest_framework import routers
from django.urls import path, include
from Backend.views import ProductList, OrderList, OrderItemsList
from .views import RegisterAPI,LoginAPI

router = routers.DefaultRouter()
router.register('product', ProductList)
router.register('order', OrderList)
router.register('item', OrderItemsList)
# router.register('register',RegisterAPI)
# router.register('login',LoginAPI)

urlpatterns = [
    path('', include(router.urls)),

]
