from knox.models import AuthToken
from rest_framework import generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import GenericAPIView
from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.pagination import PageNumberPagination

from Backend.models import Products
from Backend.serializers import ProductSerializer, RegisterSerializer, UserSerializer, LoginSerializer, \
    OrderItemsSerializers, OrderSerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from .models import Products, Orders, OrderItems

from rest_framework.views import APIView
from rest_framework.response import Response


class ProductPagination(PageNumberPagination):
    page_size = 6


class ProductList(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer
    pagination_class = ProductPagination
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ['id', 'title']


class OrderList(viewsets.ModelViewSet):
    queryset = Orders.objects.all()
    serializer_class = OrderSerializer
    search_fields = ['id', 'Title']
    filter_backends = (SearchFilter, OrderingFilter)

    # def get_queryset(self):
    #     queryset = self.queryset
    #     query_set = queryset.filter(user=self.request.user)
    #     return query_set
    #


class OrderItemsList(viewsets.ModelViewSet):
    queryset = OrderItems.objects.all()
    serializer_class = OrderItemsSerializers


class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


class LoginAPI(generics.GenericAPIView):
    serializer_class = LoginSerializer

    # permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = self.get_serializer(data=request.data)
        # serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })
