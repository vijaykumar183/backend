
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
#from rest_framework.authtoken.admin import User


# class AccountReg(models.Model):
#     username = models.CharField(max_length=50)
#     password = models.CharField(max_length=20)
#     email = models.EmailField(unique=True, max_length=100)
#     user = models.ForeignKey(User,on_delete=models.CASCADE)

# Here is Product models
class Products(models.Model):
    #id = models.IntegerField()

    title = models.CharField(max_length=30)
    Description = models.TextField(blank=True, null=True)
    Image_link = models.URLField()
    Price = models.DecimalField(max_digits=10,decimal_places=2)
    Created_At = models.DateField(auto_now_add=True)
    Updated_At = models.DateField(auto_now=True)
    #user = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.id}-{self.title}-{self.Description}-{self.Image_link}-{self.Price}-{self.Created_At}-{self.Updated_At}"

# Here is Order models
class Orders(models.Model):

    Status_method = (
        ('new','New'),
        ('paid','Paid'),
    )

    Status_Choice = (
        ('new', 'New'),
        ('paid','Paid'),
    )

    Payment_method = (
        ('cash','Cash'),
    )

    Payment_Choice = (
        ('cash', 'Cash'),
    )

    UserId = models.ForeignKey(User,on_delete=models.CASCADE)
    Total = models.FloatField()
    Created_At_Order = models.DateField()
    Update_At_Order = models.DateField()
    Status = models.CharField(max_length=30, choices=Status_Choice)
    mode_of_payment = models.CharField(max_length=30,choices=Payment_Choice)

    def __str__(self):
        return f"{self.UserId},{self.Total},{self.Created_At_Order},{self.Status},{self.mode_of_payment}"


class OrderItems(models.Model):
    Order_Id = models.ForeignKey(Orders,on_delete=models.CASCADE)
    Product_Id = models.ForeignKey(Products,on_delete=models.CASCADE)
    Quantity =models.IntegerField()
    price = models.DecimalField(decimal_places=2,max_digits=10)

    def __str__(self):
        return f"{self.Order_Id},{self.Product_Id},{self.price}"
