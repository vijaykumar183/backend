from django.contrib.auth import authenticate
from rest_framework import serializers
from django.contrib.auth.models import User
from Backend.models import Products,Orders,OrderItems


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ('title',
                  'Description',
                  'Image_link',
                  'Price',
                  'Created_At',
                  'Updated_At')


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = ('UserId',
                  'Total',
                  'Created_At_Order',
                  'Update_At_Order',
                  'Status','mode_of_payment')


class OrderItemsSerializers(serializers.ModelSerializer):
    class Meta:
        model = OrderItems
        fields = ('Order_Id','Product_Id','Quantity',
                  'price')


# class RegisterSeriliazer(serializers.Serializer):
#     username = serializers.CharField(max_length=50)
#     email = serializers.EmailField()
#     password = serializers.CharField(max_length=15)

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','email')
# Register
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','email','password')
        extra_kwargs = {'password':{'write_only':True}}

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'],validated_data['email'],validated_data['password'])
        return user

# Login
class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        user = authenticate(**data)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Incorrect Credential's")